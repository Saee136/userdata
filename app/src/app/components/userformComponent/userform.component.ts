/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { FormControl, FormGroup,Validators }   from '@angular/forms';
import { userservice} from 'app/sd-services/userservice';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

 /**
 * 
 * Serivice Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-userform',
    templateUrl: './userform.template.html'
})

export class userformComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
   userData : FormGroup;
   condition:boolean;
   users:any;
    constructor(private bdms: NDataModelService ,private userservice:userservice) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.userData=new FormGroup({
            name:new FormControl('',Validators.required),
            email:new FormControl('',Validators.required),
            mobile:new FormControl('',Validators.required)
        });
    }
    onclick(userdata){
        if(userdata.valid){
            console.log(userdata);
      this.userservice.postdata(userdata);
      alert("saved successfully!")
        }   
        else{
            alert("please fill all fields!")
        }
    
    }
    async getusers(){

        this.condition=true;
        var status=(await this.userservice.getdata()).local.result;
        console.log(status);
        this.users=this.convert(status);

    }
    convert(obj){
        return Array.from(Object.keys(obj), k => obj[k]);
    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, filter, keys, sort, pagenumber, pagesize,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete (dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
